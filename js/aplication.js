
function saveUsers(){


    if(!register()){
        return false;
    }
    

    let firstname = document.getElementById('firstname').value;
	let lastname = document.getElementById('lastname').value;
    let phone = document.getElementById('phone').value;
    let username = document.getElementById('username').value;
    let password = document.getElementById('password').value;
    let average = "";
    let aboutMe = "";
    let user = {
            firstname,
            lastname,
            phone,
            username,
            password,
            average,
            aboutMe
    };


    
    insertToLocalStorage('usuarios', user);
    validarUsuario(username, password);
    console.log(user);
    console.log('User saved');
    clear();
}

function insertToLocalStorage(formUsers, object) {
    let data = JSON.parse(localStorage.getItem(formUsers));
  
    if (!data) {
        data = [];
    }
    data.push(object);
    localStorage.setItem(formUsers, JSON.stringify(data));
    return data;
}


function obtenerListaUsuarios(){
    var listUsuarios = JSON.parse(localStorage.getItem("usuarios"));

    if(listUsuarios == null){
        listUsuarios = 
        [
          
        ]
    } 
     return listUsuarios;
}


function validarUsuario(username, password){
    let listUsuarios = obtenerListaUsuarios();
    let estado = false;

    for(let i = 0; i < listUsuarios.length; i++){
        if(listUsuarios[i].username == username && listUsuarios[i].password == password){
            estado = true;
            sessionStorage.setItem("usuarioLogueado", listUsuarios[i].firstname + ' ' + listUsuarios[i].lastname);
            
        }
    }
    return estado;
}



//Validaciones del formulario de registro
function register(){
    let firstname = document.getElementById("firstname").value;
    let lastname = document.getElementById("lastname").value;
    let phone = document.getElementById("phone").value;
    let username = document.getElementById("username").value;
    let password = document.getElementById("password").value;
    let confirm_password = document.getElementById("confirm-password").value;


    //validaciones
    if(firstname == "") {
        jQuery('#errorName').fadeIn();
        return false;
    }
    
    else if(lastname == "") {
        jQuery('#errorLastname').fadeIn();
        jQuery('#errorName').fadeOut();
        return false;
    } else {
        if(lastname != ""){
            jQuery('#errorName').fadeOut();
        }
    }

    if(phone == "") {
        jQuery('#errorPhone').fadeIn();
        jQuery('#errorLastname').fadeOut()
        return false;
    } else {
        if(phone != ""){
            jQuery('#errorLastname').fadeOut()
        }
    } 
    
    if(isNaN(phone)){
        jQuery('#errorNumero').fadeIn();
        jQuery('#errorPhone').fadeOut();
        return false;
    }

    if(username == "") {
        jQuery('#errorUsername').fadeIn();
        jQuery('#errorNumero').fadeOut();
        jQuery('#errorPhone').fadeOut()
        return false;
    } else if(username != ""){
        jQuery('#errorNumero').fadeOut();
        jQuery('#errorPhone').fadeOut();
    }

    if(password == "") {
        jQuery('#errorPassword').fadeIn();
        jQuery('#errorUsername').fadeOut();
        return false;
    } else if(password != ""){
        jQuery('#errorUsername').fadeOut();
    }

    if(confirm_password == "") {
        jQuery('#errorConfirm').fadeIn();
        jQuery('#errorPassword').fadeOut();
        return false;
    } else if(confirm_password != ""){
        jQuery('#errorConfirm').fadeOut();
        jQuery('#errorPassword').fadeOut();
    }

    if(password != confirm_password){
       jQuery('#errorCoin').fadeIn();
       return false;

    } else if(password == confirm_password){
        jQuery('#errorCoin').fadeOut();
    }

    return true;
}


function iniciarSesion(){
    var username = "";
    var password = "";
    var estado = false;
    
    username = document.querySelector("#username-log").value;
    password = document.querySelector("#password-log").value;

    estado = validarUsuario(username, password);
    console.log(estado);

    if(estado == true){
        ingresar();
    } else{
        alert("Usuario o contraseña incorrecto");
    }
}

function ingresar(){
    window.location.href = "dashboard.html";
}

//Tabla principal
function searchRide() {

    let campos = JSON.parse(localStorage.getItem("rides"));
    let from = document.getElementById('from').value;
    let to = document.getElementById('to').value;
    if (campos != null && from != null && to != null) {
        //borra los elementos de la clase trx
        jQuery(".trx").remove();
  
        for (x = 0; x < campos.length; x++) {
            if ((campos[x].start == from) && (campos[x].end == to)) {
                jQuery("#tabla-general").append(
                    "<tr class='trx'>  <td> " +
                    campos[x].user +
                    " </td> <td> " +
                    campos[x].start +
                    " </td> <td> " +
                    campos[x].end +
                    " </td> <td><button class='btn btn-link' onclick='view("+x+")'>View</button></td>  </tr>"
                );
            }
        }
    }
  
  }
  
  function view(id){
  localStorage.setItem("idDetails", id);
  
  let campos = JSON.parse(localStorage.getItem("rides"));
  
  //obtenemos el id
  id = localStorage.getItem("idDetails");
  document.getElementById("user").innerHTML = campos[id].user;
  document.getElementById("nameRide").innerHTML = campos[id].nameRide;
  document.getElementById("start").innerHTML = campos[id].start;
  document.getElementById("end").innerHTML = campos[id].end;
  document.getElementById("description").innerHTML = campos[id].description;
  document.getElementById("departure").innerHTML = campos[id].departure;
  document.getElementById("arrival").innerHTML = campos[id].arrival;
  document.getElementById("days").innerHTML = campos[id].days;
  jQuery('#DetalleModal').modal('show');
  
  }


function clear() { 
    jQuery("#name").val("");
    jQuery("#lastname").val("");
    jQuery("#phone").val("");
    jQuery("#username").val("");
    jQuery("#password").val("");
    jQuery("#confirm-password").val("");
}

function bindEvents() {
    jQuery('#register-boton').bind('click', registerButtonHandler);
    jQuery('#login').bind('click', loginButtonHandler);
    jQuery('#settings').bind('click', updateDataUsers);
    jQuery('#filter').bind('click', searchRideButton);
}

function registerButtonHandler(element) {
    saveUsers();
}
function loginButtonHandler(element) {
   iniciarSesion();
}

function updateDataUsers(element){
    updateUsers();
}

function searchRideButton(element){
    searchRide();
}

bindEvents();