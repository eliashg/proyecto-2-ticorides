tableForUsers();
logueado();
function saveRides (){
  
  if(!validar()){
    return false;
  } 
    let user = sessionStorage.getItem('usuarioLogueado');
    
    let days = [];
    let nameRide = document.getElementById('nameRide').value;
    let start = document.getElementById('start').value;
    let end = document.getElementById('end').value;
    let description = document.getElementById('description').value;
    let departure = document.getElementById('departure').value;
    let arrival = document.getElementById('arrival').value;
      $("input[type=checkbox]:checked").each(function(){
        days.push(this.value);
         
      });
     
  
       ride = {
          nameRide,
          start,
          end,
          description,
          departure,
          arrival,
          days,
          user
    };
  
      insertToLocalStorage('rides', ride);
      console.log(ride);
      console.log('Ridde saved');
    
    dash();
    clear();

      tableForUsers();
}

function insertToLocalStorage(formUsers, object) {
    let data = JSON.parse(localStorage.getItem(formUsers));
  
    if (!data) {
        data = [];
    }
    data.push(object);
    localStorage.setItem(formUsers, JSON.stringify(data));
    return data;
}

function tableForUsers() {
    //convierte el string en arreglo
    log = sessionStorage.getItem("usuarioLogueado");
    campos = JSON.parse(localStorage.getItem("rides"));
    if (campos != null) {
      //borra los elementos de la clase trx
      jQuery(".trx").remove();
  
      for (x = 0; x < campos.length; x++) {
        if (campos[x].user == log) {
          jQuery("#table-dash").append(
            "<tr class='trx'>  <td> " +
            campos[x].nameRide +
            " </td> <td> " +
            campos[x].start +
            " </td> <td> " +
            campos[x].end +
            " </td> <td><button class='btn btn-link' onclick='editRide(" + x + ")'>Edit</button> <button class='btn btn-link' onclick='removeFromLocalStorage(" + x + ")'>Delete</button></td>  </tr>"
          );
        }
  
      }
    } 
}

function editRide(id){
  
    //almacena el id en la bd
    localStorage.setItem("idForm", id);
    window.location.href = ('ride-edit.html?id' + '=' + id);
  
}

function removeFromLocalStorage(index){
    var rides = [],
        dataInLocalStorage = localStorage.getItem('rides');
    
    rides = JSON.parse(dataInLocalStorage);
    if(confirm("¿Está seguro de que desea borrar este Ride?")){
      rides.splice(index, 1);
    
      localStorage.setItem('rides', JSON.stringify(rides));
    
      tableForUsers();
    }
    
}


function addRide(){
    window.location.href = ('ride-add.html');
}

function dash(){
  window.location.href = ('dashboard.html');
}

function validar(){

    let nameRide = document.getElementById('nameRide').value;
    let start = document.getElementById('start').value;
    let end = document.getElementById('end').value;
    let description = document.getElementById('description').value;
    let departure = document.getElementById('departure').value;
    let arrival = document.getElementById('arrival').value;


if(nameRide == "") {
  alert("Debe digitar el nombre del ride");
  return false;
}

else if(start == "") {
  alert("Debe digitar el inicio del ride");
    return false;
}


if(end == "") {
  alert("Debe digitar el final del ride");
    return false;
}


if(description == "") {
  alert("Debe digitar la descripción del ride");
  return false;
} 

if(departure == "") {
  alert("Debe digitar la hora de partida del ride");
  return false;
} 

if(arrival == "") {
  alert("Debe digitar la hora de llegada del ride");
  return false;
}

return true;

}

// function findUsers(){
//   let campos = JSON.parse(localStorage.getItem("usuarios"));
//   for(x = 0; x < campos.length; x++){
//       campos[x].fullname;
//       campos[x].average;
//       campos[x].aboutMe;
//       changeData(x);
//   }

// }

// function changeData(index){
//   localStorage.setItem("idChange", id);
//   window.location.href = ('settings.html?id' + '=' + index);
// }

function logueado(){

  let userLogin = sessionStorage.getItem('usuarioLogueado');

  document.getElementById("logueado").innerHTML = userLogin;
  

}

function bindEvents() {
    jQuery("#save-boton").bind("click", guardarButtonHandler);
    jQuery('#boton-add').bind('click', addButtonHandler);
   
  }
  
  
  function guardarButtonHandler(element) {
    saveRides();

  }
  
  function addButtonHandler(element) {
    addRide();
  }
  
  bindEvents();