if (localStorage.getItem("rides") != null) {
    campos = JSON.parse(localStorage.getItem("rides"));
 }
 
//   if (localStorage.getItem("usuarios") != null) {
//     datos = JSON.parse(localStorage.getItem("usuarios"));
//  }
 
 id = localStorage.getItem("idForm");
 let newName = document.querySelector('#name-edit').value = campos[id].nameRide;
 let newStart = document.querySelector('#start-edit').value = campos[id].start;
 let newEnd = document.querySelector('#end-edit').value = campos[id].end;
 let newDescription = document.querySelector('#description-edit').value = campos[id].description;
 let newDeparture =  document.querySelector('#departure-edit').value = campos[id].departure;
 let newArrival = document.querySelector('#arrival-edit').value = campos[id].arrival;
//  let newDays = document.querySelector('#check-edit').value = campos[id].days;
 
 

function getUsers(){
  let newFullName =  document.querySelector('#fullname').value;
  let newAverage = document.querySelector('#average').value;
  let newAboutMe = document.querySelector('#aboutMe').value;
  
  
  campos = JSON.parse(localStorage.getItem("usuarios"));
  if (campos != null) {

    for (x = 0; x < campos.length; x++) {

      campos[x].name = newFullName;
      campos[x].average = newAverage;
      campos[x].aboutMe = newAboutMe;
      
    }
  }    
}


 
//  tableForUsers();
 function editRideLocalStorage() {
   let user = sessionStorage.getItem('usuarioLogueado');
   let days = [];
   let nameRide = document.querySelector('#name-edit').value;
   let start = document.querySelector('#start-edit').value;
   let end = document.querySelector('#end-edit').value;
   let description = document.querySelector('#description-edit').value;
   let departure = document.querySelector('#departure-edit').value;
   let arrival = document.querySelector('#arrival-edit').value;
   $("input[type=checkbox]:checked").each(function(){
      days.push(this.value);
   });
 
 
 
  newName[id] = nameRide;
  newStart[id] = start;
  newEnd[id] = end;
  newDescription[id] = description;
  newDeparture[id] = departure;
  newArrival[id] = arrival;
  // newDays[id] = days;
 
 
  let ride = {
    nameRide,
    start,
    end,
    description,
    departure,
    arrival,
    days,
    user
 };
 
 insertToLocalStorage('rides', ride);
 removeFromEdit(id);
 dash();
  console.log(ride);
  tableForUsers();
 
 }
 function removeFromEdit(id){
   var rides = [],
       dataInLocalStorage = localStorage.getItem('rides');
   
   rides = JSON.parse(dataInLocalStorage);
     rides.splice(id, 1);
   
     localStorage.setItem('rides', JSON.stringify(rides));
   
     tableForUsers();
   
   
 }
 
 
 
 function bindEvents() {
   jQuery('#boton-edit').bind('click', editButtonHandler);
 }
 
 
 function editButtonHandler(element) {
 editRideLocalStorage();
 }
 
 bindEvents();